package modele.plateau;

public class PoutreHorizontale extends EntiteStatique {
	public static int ID = 3;

	public PoutreHorizontale(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	@Override
	public boolean peutEtreEcrase() {
		return false;
	}

	@Override
	public boolean peutServirDeSupport() {
		return true;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return false;
	}

	@Override
	public boolean peutEtreRamassee() {
		return false;
	}

	@Override
	public boolean peutEtreTraversee() {
		return false;
	}

	@Override
	public boolean estUnPersonnage() {
		return false;
	}
}
