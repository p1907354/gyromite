package modele.plateau;

import modele.deplacements.Direction;

/**
 * Entités amenées à bouger (colonnes, ennemis)
 */
public abstract class EntiteDynamique extends Entite {
	public static int ID = 1;
	boolean toucheLeSol = false;

	private Entite entiteCachee = null;

	public EntiteDynamique(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	public boolean avancerDirectionChoisie(Direction d) {
		return jeu.deplacerEntite(this, d);
	}

	public Entite regarderDansLaDirection(Direction d) {
		return jeu.regarderDansLaDirection(this, d);
	}

	public Entite getEntiteCachee() {
		return entiteCachee;
	}

	public void setEntiteCachee(Entite entite) {
		entiteCachee = entite;
	}

	public boolean toucheLeSol() {
		return toucheLeSol;
	}

	public void setToucheLeSol(boolean toucheLeSol) {
		this.toucheLeSol = toucheLeSol;
	}
}
