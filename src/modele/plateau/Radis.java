package modele.plateau;

public class Radis extends EntiteStatique {
	public static int ID = 7;

	public Radis(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	@Override
	public boolean peutEtreEcrase() {
		return false;
	}

	@Override
	public boolean peutServirDeSupport() {
		return false;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return false;
	};

	@Override
	public boolean peutEtreRamassee() {
		return true;
	}

	@Override
	public boolean peutEtreTraversee() {
		return true;
	}

	@Override
	public boolean estUnPersonnage() {
		return false;
	}
}
