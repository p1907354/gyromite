/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

/**
 * Héros du jeu
 */
public class Heros extends EntiteDynamique {
	public static int ID = 10;

	private Entite entiteCachee = null;
	private boolean direction = false;
	private boolean toucheLeSol = true;
	private boolean porteUnRadis = false;
	private boolean mort = false;

	public Heros(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	@Override
	public boolean peutEtreEcrase() {
		return true;
	}

	@Override
	public boolean peutServirDeSupport() {
		return true;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return false;
	}

	@Override
	public boolean peutEtreRamassee() {
		return false;
	}

	@Override
	public boolean peutEtreTraversee() {
		return true;
	}

	@Override
	public boolean estUnPersonnage() {
		return true;
	}

	public Entite getEntiteCachee() {
		return entiteCachee;
	}

	public void setEntiteCachee(Entite entite) {
		entiteCachee = entite;
	}

	public boolean getDirection() {
		return direction;
	}

	public void setDirection(boolean direction) {
		this.direction = direction;
	}

	public boolean toucheLeSol() {
		return toucheLeSol;
	}

	public void setToucheLeSol(boolean toucheLeSol) {
		this.toucheLeSol = toucheLeSol;
	}

	public boolean porteUnRadis() {
		return porteUnRadis;
	}

	public void setPorteUnRadis(boolean porteUnRadis) {
		this.porteUnRadis = porteUnRadis;
	}

	public boolean estMort() {
		return mort;
	}

	public void setMort(boolean mort) {
		this.mort = mort;
		System.out.println("Hector muerto!");
	}

	public int getSuperIdentifiant() {
		return super.getIdentifiant();
	}

}
