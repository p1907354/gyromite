package modele.plateau;

public class ComposantColonne extends EntiteDynamique {
	public static int ID = 9;

	private String couleurStr;

	public ComposantColonne(Jeu _jeu, String _couleurStr) {
		super(_jeu);
		couleurStr = _couleurStr;
		identifiant = ID;
	}

	@Override
	public boolean peutEtreEcrase() {
		return false;
	}

	@Override
	public boolean peutServirDeSupport() {
		return true;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return false;
	}

	@Override
	public boolean peutEtreRamassee() {
		return false;
	}

	@Override
	public boolean peutEtreTraversee() {
		return false;
	}

	@Override
	public boolean estUnPersonnage() {
		return false;
	}

	public String getCouleurStr() {
		return couleurStr;
	}
}
