package modele.plateau;

public class Corde extends EntiteStatique {
	public static int ID = 6;

	public Corde(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	@Override
	public boolean peutEtreEcrase() {
		return false;
	}

	@Override
	public boolean peutServirDeSupport() {
		return true;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return true;
	}

	@Override
	public boolean peutEtreRamassee() {
		return false;
	}

	@Override
	public boolean peutEtreTraversee() {
		return true;
	}

	@Override
	public boolean estUnPersonnage() {
		return false;
	}
}
