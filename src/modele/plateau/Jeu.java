/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import modele.deplacements.Controle4Directions;
import modele.deplacements.DeplacementColonneBleue;
import modele.deplacements.DeplacementColonneRouge;
import modele.deplacements.Direction;
import modele.deplacements.Gravite;
import modele.deplacements.IA;
import modele.deplacements.Ordonnanceur;

/**
 * Actuellement, cette classe gère les postions (ajouter conditions de victoire,
 * chargement du plateau, etc.)
 */
public class Jeu {
	private int tailleX;
	private int tailleY;

	private int nombreDeBombes;
	private int score;
	private int compteARebourd;
	// private int meilleurScore;

	// compteur de déplacements horizontal et vertical (1 max par défaut, à chaque
	// pas de temps)
	private HashMap<Entite, Integer> cmptDeplH = new HashMap<>();
	private HashMap<Entite, Integer> cmptDeplV = new HashMap<>();

	private Heros hector;

	private HashMap<Entite, Point> map = new HashMap<>(); // permet de récupérer la position d'une entité à partir de sa
															// référence
	private Entite[][] grilleEntites; // permet de récupérer une entité à partir de ses coordonnées
	private HashMap<String, ArrayList<Colonne>> mapColonnes = new HashMap<>();

	private Ordonnanceur ordonnanceur = new Ordonnanceur(this);

	private int nombreDeNiveauxJoues;
	private String cheminFichier;
	private int nombreDeNiveau;
	private int meilleurScore;

	public Jeu() {
		creationFichierMeilleurScore();
		lireMeilleurScoreDansFichier();
		nombreDeNiveau = 3;
		cheminFichier = setCheminFichier();
		try {
			nombreDeBombes = 0;
			initialisationDesEntites(cheminFichier);
		} catch (IOException e) {
			e.printStackTrace();
		}

		initialisationDesColonnes();
		score = 0;
		nombreDeNiveauxJoues = 0;
		compteARebourd = 1000;
	}

	public void majMeilleurScore() {
		if (score > meilleurScore)
			meilleurScore = score;
	}

	private void creationFichierMeilleurScore() {
		try {
			File f = new File("data/meilleurScore.txt");
			if (!f.exists()) {
				BufferedWriter bw = new BufferedWriter(new FileWriter("data/meilleurScore.txt"));
				bw.write("0");
				bw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void lireMeilleurScoreDansFichier() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("data/meilleurScore.txt"));
			meilleurScore = Integer.parseInt(br.readLine());
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void ecrireMeilleurScoreDansFichier() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("data/meilleurScore.txt"));
			bw.write(meilleurScore + "");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String setCheminFichier() {
		return "data/Levels/level" + (nombreDeNiveauxJoues % nombreDeNiveau) + ".txt";
	}

	public void nextLevel() {
		Controle4Directions.getInstance().resetDirection();
		Controle4Directions.getInstance().resetlstEntitesDynamiques();
		DeplacementColonneBleue.getInstance().resetDirection();
		DeplacementColonneRouge.getInstance().resetDirection();
		DeplacementColonneBleue.getInstance().resetMapLstEntitesColonnes();
		DeplacementColonneRouge.getInstance().resetMapLstEntitesColonnes();

		cheminFichier = setCheminFichier();
		try {
			nombreDeBombes = 0;
			initialisationDesEntites(cheminFichier);
		} catch (IOException e) {
			e.printStackTrace();
		}
		initialisationDesColonnes();
		compteARebourd = 1000;
	}

	public void resetGame() {
		ecrireMeilleurScoreDansFichier();
		Controle4Directions.getInstance().resetDirection();
		Controle4Directions.getInstance().resetlstEntitesDynamiques();
		DeplacementColonneBleue.getInstance().resetDirection();
		DeplacementColonneRouge.getInstance().resetDirection();
		DeplacementColonneBleue.getInstance().resetMapLstEntitesColonnes();
		DeplacementColonneRouge.getInstance().resetMapLstEntitesColonnes();

		hector = null;
		System.out.println(hector);
		score = 0;
		nombreDeNiveauxJoues = 0;
		cheminFichier = setCheminFichier();
		try {
			nombreDeBombes = 0;
			initialisationDesEntites(cheminFichier);
		} catch (IOException e) {
			e.printStackTrace();
		}
		initialisationDesColonnes();

		compteARebourd = 1000;
	}

	public void resetCmptDepl() {
		cmptDeplH.clear();
		cmptDeplV.clear();
	}

	public void start(long _pause) {
		ordonnanceur.start(_pause);
	}

	public Entite[][] getGrille() {
		return grilleEntites;
	}

	public Heros getHector() {
		return hector;
	}

	public boolean niveauTerminee() {
		return nombreDeBombes == 0;
	}

	public boolean gameOver() {
		return hector.estMort() || (compteARebourd == 0 && nombreDeBombes > 0);
	}

	private void initialisationDesColonnes() {
		mapColonnes.put("Rouge", new ArrayList<>());
		mapColonnes.put("Bleu", new ArrayList<>());
		for (int x = 0; x < tailleX; x++) {
			for (int y = 0; y < tailleY; y++) {
				if (grilleEntites[x][y] != null) {
					if (grilleEntites[x][y].getIdentifiant() == ComposantColonne.ID) {
						ComposantColonne cc = (ComposantColonne) grilleEntites[x][y];
						String couleurStr = "";
						if (cc.getCouleurStr() == "Bleu")
							couleurStr = "Bleu";
						else
							couleurStr = "Rouge";
						ArrayList<ComposantColonne> listeColonne = new ArrayList<>();

						listeColonne.add(cc);

						Direction direction = null;

						if ((x - 1 >= 0 && grilleEntites[x - 1][y] != null
								&& grilleEntites[x - 1][y].getIdentifiant() == Connecteur.ID)
								|| (x + 1 < tailleX && grilleEntites[x + 1][y] != null
										&& grilleEntites[x + 1][y].getIdentifiant() == Connecteur.ID))
							direction = Direction.bas;
						else
							direction = Direction.haut;

						y++;
						if (y < tailleY) {
							while (grilleEntites[x][y] != null
									&& grilleEntites[x][y].getIdentifiant() == ComposantColonne.ID
									&& (cc).getCouleurStr() == couleurStr) {
								listeColonne.add((ComposantColonne) grilleEntites[x][y]);
								y++;
							}
						}

						y--;

						Colonne colonne = new Colonne(listeColonne, direction);

						if (couleurStr == "Bleu") {
							DeplacementColonneBleue.getInstance().addEntiteColonne(colonne, couleurStr);
							ordonnanceur.add(DeplacementColonneBleue.getInstance());
						} else {
							DeplacementColonneRouge.getInstance().addEntiteColonne(colonne, couleurStr);
							ordonnanceur.add(DeplacementColonneRouge.getInstance());
						}

						mapColonnes.get(couleurStr).add(colonne);
					}
				}
			}
		}
	}

	private void initialisationDesEntites(String cheminFichier) throws IOException {

		File fichier = new File(cheminFichier);

		Scanner lecteur = new Scanner(fichier);
		String[] mots = null;
		if (lecteur.hasNextLine()) {
			mots = lecteur.nextLine().split(" ");
			tailleX = Integer.parseInt(mots[0]);
			tailleY = Integer.parseInt(mots[1]);
			grilleEntites = new Entite[tailleX][tailleY];

			Gravite g = new Gravite();
			int ligne = 0;
			while (lecteur.hasNextLine()) {
				mots = lecteur.nextLine().split("");
				int colonne = 0;
				for (String c : mots) {
					// System.out.println("colonne/ligne: " + colonne + " " + ligne);

					switch (c) {
						case "*":
							addEntite(new PoutreHorizontale(this), colonne, ligne);
							break;
						case "#":
							addEntite(new PoutreVerticale(this), colonne, ligne);
							break;
						case "_":
							addEntite(new Sol(this), colonne, ligne);
							break;
						case "|":
							addEntite(new Corde(this), colonne, ligne);
							break;
						case "R":
							addEntite(new Radis(this), colonne, ligne);
							break;
						case "+":
							addEntite(new Connecteur(this), colonne, ligne);
							break;
						case "x":
							addEntite(new ComposantColonne(this, "Bleu"), colonne, ligne);
							break;
						case "=":
							addEntite(new ComposantColonne(this, "Rouge"), colonne, ligne);
							break;
						case "H":
							hector = new Heros(this);
							addEntite(hector, colonne, ligne);

							g.addEntiteDynamique(hector);
							ordonnanceur.add(g);

							Controle4Directions.getInstance().addEntiteDynamique(hector);
							ordonnanceur.add(Controle4Directions.getInstance());
							break;
						case "B":
							addEntite(new Bombe(this), colonne, ligne);
							nombreDeBombes++;
							break;
						case "S":
							Bot bot = new Bot(this);
							addEntite(bot, colonne, ligne);
							g.addEntiteDynamique(bot);

							// IA ia = new IA();
							// ia.addEntiteDynamique(bot);
							// ordonnanceur.add(ia);
							ordonnanceur.add(g);
							break;
						default:
							addEntite(null, colonne, ligne);
					}
					colonne++;
				}
				ligne++;
			}
		} else {
			System.err.println("Fichier vide");
		}

		lecteur.close();
	}

	private void addEntite(Entite e, int x, int y) {
		grilleEntites[x][y] = e;
		map.put(e, new Point(x, y));
	}

	/**
	 * Permet par exemple a une entité de percevoir sont environnement proche et de
	 * définir sa stratégie de déplacement
	 *
	 */
	public Entite regarderDansLaDirection(Entite e, Direction d) {
		Point positionEntite = map.get(e);
		return objetALaPosition(calculerPointCible(positionEntite, d));
	}

	/**
	 * Si le déplacement de l'entité est autorisé (pas de mur ou autre entité), il
	 * est réalisé Sinon, rien n'est fait.
	 */
	public boolean deplacerEntite(Entite e, Direction d) {
		boolean retour = false;

		Point pCourant = map.get(e);

		Point pCible = calculerPointCible(pCourant, d);

		// System.out.println("Entite en question: " + e);
		// System.out.println("Direction en question: " + d);

		/**
		 * Tout ce qui est dynamique peut se deplacer sur une case de ramassable Si
		 * heros -> bombe: elle disparait, radis: il le tient dans la main Si Bot ->
		 * bombe: rien, radis: ??
		 */
		if ((contenuDansGrille(pCible) && objetALaPosition(pCible) == null) || (objetALaPosition(pCible) != null
				&& (objetALaPosition(pCible).peutEtreRamassee() || objetALaPosition(pCible).peutEtreTraversee()))) {
			switch (d) {
				case bas:
				case haut:
					if (cmptDeplV.get(e) == null) {
						cmptDeplV.put(e, 1);
						retour = true;
					}
					break;
				case gauche:
				case droite:
					if (cmptDeplH.get(e) == null) {
						cmptDeplH.put(e, 1);
						retour = true;

					}
					break;
			}
		}

		if (retour) {
			deplacerEntite(pCourant, pCible, e);
		}

		return retour;
	}

	private Point calculerPointCible(Point pCourant, Direction d) {
		Point pCible = null;

		switch (d) {
			case haut:
				pCible = new Point(pCourant.x, pCourant.y - 1);
				break;
			case bas:
				pCible = new Point(pCourant.x, pCourant.y + 1);
				break;
			case gauche:
				pCible = new Point(pCourant.x - 1, pCourant.y);
				break;
			case droite:
				pCible = new Point(pCourant.x + 1, pCourant.y);
				break;

		}

		return pCible;
	}

	private void deplacerEntite(Point pCourant, Point pCible, Entite e) {
		if (e != null && e instanceof EntiteDynamique) {
			EntiteDynamique ed = (EntiteDynamique) e;
			Entite tampon = ed.getEntiteCachee();
			grilleEntites[pCourant.x][pCourant.y] = tampon;
			map.put(tampon, pCourant);
			Entite entiteCible = grilleEntites[pCible.x][pCible.y];

			if (entiteCible != null) {
				if (ed.getIdentifiant() == ComposantColonne.ID) {
					if (entiteCible.estUnPersonnage()) {
						EntiteDynamique ecd = (EntiteDynamique) entiteCible;
						if (!ecd.avancerDirectionChoisie(Direction.haut)) {
							if (entiteCible == hector) {
								hector.setMort(true);
							}
						}
						ed.setEntiteCachee(null);
					} else
						ed.setEntiteCachee(entiteCible);

					// HERO PART
				} else if (ed == hector) {
					Heros eh = (Heros) e;
					if (entiteCible.getIdentifiant() == Bombe.ID) {
						--nombreDeBombes;
						ed.setEntiteCachee(null);
					} else if (entiteCible.getIdentifiant() == Radis.ID) {
						if (!eh.porteUnRadis()) {
							eh.setPorteUnRadis(true);
							ed.setEntiteCachee(null);
						} else {
							ed.setEntiteCachee(entiteCible);
						}
					} else
						ed.setEntiteCachee(entiteCible);

					// BOT PART
				} else if (e.getIdentifiant() == Bot.ID) {

					Bot eb = (Bot) e;

					// BOT WITH CORDE
					if (entiteCible.getIdentifiant() == Corde.ID) {

						eb.setEntiteCachee(entiteCible);

						// BOT WITH BOMBE
					} else if (entiteCible.getIdentifiant() == Bombe.ID) {

						eb.setEntiteCachee(entiteCible);

						// BOT WITH HECTOR
					} else if (entiteCible == hector) {

						hector.setMort(true);
					}
					// BOT WITH RADIS
					else if (entiteCible.getIdentifiant() == Radis.ID) {
						eb.setEntiteCachee(null);
						eb.ralentirBot();

					} else {
						ed.setEntiteCachee(null);
					}
				}
			} else {
				ed.setEntiteCachee(null);
			}
			grilleEntites[pCible.x][pCible.y] = e;
			map.put(e, pCible);
		} else {
			System.out.println("Entite nulle ou non dynamique");
		}
	}

	/**
	 * Indique si p est contenu dans la grille
	 */
	public boolean contenuDansGrille(Point p) {
		return p.x >= 0 && p.x < tailleX && p.y >= 0 && p.y < tailleY;
	}

	private Entite objetALaPosition(Point p) {
		Entite retour = null;

		if (contenuDansGrille(p)) {
			retour = grilleEntites[p.x][p.y];
		}
		return retour;
	}

	/**
	 * @param e Entite
	 * @return position d'une entite e
	 */
	// private Point positionEntiteActuelle(Entite e) { // for debugging

	// Point pCourant = map.get(e);

	// // System.out.println(e + ": " + pCourant.x + "," + pCourant.y);
	// return pCourant;
	// }

	// private void ramasserObjet() {

	// }

	public Ordonnanceur getOrdonnanceur() {
		return ordonnanceur;
	}

	public int getTailleX() {
		return tailleX;
	}

	public int getTailleY() {
		return tailleY;
	}

	public int getCompteARebourd() {
		return compteARebourd;
	}

	public void decrementeCompteARebourd() {
		compteARebourd--;
	}

	public int getNombreDeBombes() {
		return nombreDeBombes;
	}

	public void setNombreDeBombes(int nombreDeBombes) {
		this.nombreDeBombes = nombreDeBombes;
	}

	public int getScore() {
		return score;
	}

	public int getMeilleurScore() {
		return meilleurScore;
	}

	public void incrementeScore() {
		score++;
		majMeilleurScore();
	}

	public String getCheminFichier() {
		return cheminFichier;
	}

	public void setCheminFichier(String cheminFichier) {
		this.cheminFichier = cheminFichier;
	}

	public void incrementNombreDeNiveauxJoues() {
		nombreDeNiveauxJoues++;
	}
}

/*
 * stocker le nombre de bombe dans le niveau set une valeur de Time que l'on
 * décrémentera à chaque passage de thread si toute les bombes ont étés
 * ramassées alors incrémenter de Time le score et relancer le niveau
 * 
 * 
 * 
 * 
 * 
 */
