/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import java.util.Random;

import modele.deplacements.Direction;

// import java.util.Random;

/**
 * Ennemis (Smicks)
 */
public class Bot extends EntiteDynamique {
	public static int ID = 12;
	public int speed = 150;

	private Direction direction = Direction.droite;

	public Bot(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	public void switchDirection() {
		if (direction == Direction.gauche) {
			direction = Direction.droite;
		} else if (direction == Direction.droite) {
			direction = Direction.gauche;
		}

		if (direction == Direction.haut) {
			direction = Direction.bas;
		} else if (direction == Direction.bas) {
			direction = Direction.haut;
		}
	}

	public Direction getRandomDirection() {
		Direction[] values = Direction.values();
		int index = new Random().nextInt(values.length);
		Direction randomDirection = values[index];

		return randomDirection;
	}

	public void ralentirBot() {

		this.speed = 0;
		
		// Sleep for 10 seconds (10000 milliseconds)
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				System.out.println(ex);
			}
		}
	}

	@Override
	public boolean peutEtreEcrase() {
		return true;
	}

	@Override
	public boolean peutServirDeSupport() {
		return true;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return false;
	};

	@Override
	public boolean peutEtreRamassee() {
		return false;
	}

	@Override
	public boolean peutEtreTraversee() {
		return false;
	}

	@Override
	public boolean estUnPersonnage() {
		return true;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}
}
