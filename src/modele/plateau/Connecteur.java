package modele.plateau;

public class Connecteur extends EntiteStatique {
	public static int ID = 8;

	public Connecteur(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	@Override
	public boolean peutEtreEcrase() {
		return false;
	}

	@Override
	public boolean peutServirDeSupport() {
		return true;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return false;
	}

	@Override
	public boolean peutEtreRamassee() {
		return false;
	}

	@Override
	public boolean peutEtreTraversee() {
		return false;
	}

	@Override
	public boolean estUnPersonnage() {
		return false;
	}
}
