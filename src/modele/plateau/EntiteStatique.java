package modele.plateau;

/**
 * Ne bouge pas (murs...)
 */
public abstract class EntiteStatique extends Entite {
	public static int ID = 2;

	public EntiteStatique(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}
}