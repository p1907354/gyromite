package modele.plateau;

import java.util.ArrayList;

import modele.deplacements.Direction;

public class Colonne {
	private Direction direction;
	private ArrayList<ComposantColonne> composants;

	public Colonne(ArrayList<ComposantColonne> composants, Direction direction) {
		this.composants = composants;
		this.direction = direction;
	}

	public ArrayList<ComposantColonne> getComposants() {
		return composants;
	}

	public int getNbComposants() {
		return composants.size();
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}
}
