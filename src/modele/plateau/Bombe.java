package modele.plateau;

public class Bombe extends EntiteStatique {
	public static int ID = 11;

	public Bombe(Jeu _jeu) {
		super(_jeu);
		identifiant = ID;
	}

	@Override
	public boolean peutEtreEcrase() {
		return false;
	}

	@Override
	public boolean peutServirDeSupport() {
		return false;
	}

	@Override
	public boolean peutPermettreDeMonterDescendre() {
		return false;
	};

	@Override
	public boolean peutEtreRamassee() {
		return true;
	}

	@Override
	public boolean peutEtreTraversee() {
		return true;
	}

	@Override
	public boolean estUnPersonnage() {
		return false;
	}
}
