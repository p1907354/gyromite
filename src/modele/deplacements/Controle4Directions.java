package modele.deplacements;

import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;
import modele.plateau.Heros;

/**
 * Controle4Directions permet d'appliquer une direction (connexion avec le
 * clavier) à un ensemble d'entités dynamiques
 */
public class Controle4Directions extends RealisateurDeDeplacement {
	private Direction directionCourante;
	// Design pattern singleton
	private static Controle4Directions c3d;

	public static Controle4Directions getInstance() {
		if (c3d == null) {
			c3d = new Controle4Directions();
		}
		return c3d;
	}

	public void setDirectionCourante(Direction _directionCourante) {
		directionCourante = _directionCourante;
	}

	public boolean realiserDeplacement() {
		boolean ret = false;
		for (EntiteDynamique e : lstEntitesDynamiques) {
			if (directionCourante != null) {
				switch (directionCourante) {
					case gauche:
						if (e.estUnPersonnage() && ((Heros) e).getDirection())
							((Heros) e).setDirection(false);
						if (e.avancerDirectionChoisie(directionCourante))
							ret = true;
						break;
					case droite:
						if (e.getIdentifiant() == Heros.ID && !((Heros) e).getDirection())
							((Heros) e).setDirection(true);
						if (e.avancerDirectionChoisie(directionCourante))
							ret = true;
						break;

					case haut:
						// on ne peut pas sauter sans prendre appui
						// (attention, test d'appui réalisé à partir de la position courante, si la
						// gravité à été appliquée, il ne s'agit pas de la position affichée,
						// amélioration possible)
						if (e.getIdentifiant() == Heros.ID && ((Heros) e).toucheLeSol()) {
							System.out.println("haut");
							((Heros) e).setToucheLeSol(false);
						}
						Entite eBas = e.regarderDansLaDirection(Direction.bas);
						Entite eHaut = e.regarderDansLaDirection(Direction.haut);
						Entite eCachee = e.getEntiteCachee();
						if (((eBas != null && eBas.peutServirDeSupport())
								|| (eCachee != null && eCachee.peutPermettreDeMonterDescendre()))
								&& (eHaut == null || eHaut.peutPermettreDeMonterDescendre()
										|| eHaut.peutEtreRamassee())) {
							if (e.avancerDirectionChoisie(Direction.haut))
								ret = true;
						}
						break;
					case bas:
						eBas = e.regarderDansLaDirection(Direction.bas);
						Entite entiteCachee = e.getEntiteCachee();
						if (entiteCachee != null && entiteCachee.peutPermettreDeMonterDescendre()
								&& (eBas == null || eBas.peutEtreRamassee() || eBas.peutPermettreDeMonterDescendre())) {
							if (e.avancerDirectionChoisie(Direction.bas))
								ret = true;
						}
						break;
				}
			}
		}
		return ret;
	}

	public void resetDirection() {
		directionCourante = null;
	}
}
