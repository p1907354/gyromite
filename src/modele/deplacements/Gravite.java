package modele.deplacements;

import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

public class Gravite extends RealisateurDeDeplacement {
	@Override
	public boolean realiserDeplacement() {
		boolean ret = false;

		for (EntiteDynamique e : lstEntitesDynamiques) {
			Entite eBas = e.regarderDansLaDirection(Direction.bas);
			Entite eCachee = e.getEntiteCachee();
			if (eCachee == null || !eCachee.peutPermettreDeMonterDescendre()) {
				if (e.estUnPersonnage()) {
					if ((eBas == null || (eBas != null && !eBas.peutServirDeSupport()))) {
						if (e.toucheLeSol())
							e.setToucheLeSol(false);
						if (e.avancerDirectionChoisie(Direction.bas))
							ret = true;
					} else if (!e.toucheLeSol()) {
						e.setToucheLeSol(true);
					}
				}
			}
		}
		return ret;
	}
}
