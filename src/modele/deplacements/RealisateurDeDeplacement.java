package modele.deplacements;

import java.util.ArrayList;
import java.util.HashMap;

import modele.plateau.Colonne;
import modele.plateau.EntiteDynamique;

/**
 * Tous les déplacement sont déclenchés par cette classe (gravité, controle
 * clavier, IA, etc.)
 */
public abstract class RealisateurDeDeplacement {
	protected ArrayList<EntiteDynamique> lstEntitesDynamiques = new ArrayList<EntiteDynamique>();
	protected HashMap<String, ArrayList<Colonne>> mapLstEntitesColonnes = new HashMap<>();

	public RealisateurDeDeplacement() {
		mapLstEntitesColonnes.put("Rouge", new ArrayList<>());
		mapLstEntitesColonnes.put("Bleu", new ArrayList<>());
	}

	protected abstract boolean realiserDeplacement();

	public void addEntiteDynamique(EntiteDynamique ed) {
		lstEntitesDynamiques.add(ed);
	};

	public void addEntiteColonne(Colonne c, String couleurStr) {
		mapLstEntitesColonnes.get(couleurStr).add(c);
	};

	public void resetMapLstEntitesColonnes() {
		mapLstEntitesColonnes.replace("Rouge", new ArrayList<>());
		mapLstEntitesColonnes.replace("Bleu", new ArrayList<>());
	}

	public void resetlstEntitesDynamiques() {
		lstEntitesDynamiques = new ArrayList<EntiteDynamique>();
	}

}
