package modele.deplacements;

import modele.plateau.Jeu;

import java.util.ArrayList;
import java.util.Observable;

import static java.lang.Thread.*;

@SuppressWarnings("deprecation")
public class Ordonnanceur extends Observable implements Runnable {
	private Jeu jeu;
	private ArrayList<RealisateurDeDeplacement> lstDeplacements = new ArrayList<RealisateurDeDeplacement>();
	private long pause;
	private int cpt;

	// private static Ordonnanceur ord;
	//
	// public static Ordonnanceur getInstance(Jeu _jeu) {
	// if (ord == null)
	// ord = new Ordonnanceur(_jeu);
	//
	// return ord;
	// }
	//
	// public static Ordonnanceur newInstance(Jeu _jeu) {
	// return new Ordonnanceur(_jeu);
	// }

	private void resetLstDeplacements() {
		lstDeplacements = new ArrayList<>();
	}

	public void add(RealisateurDeDeplacement deplacement) {
		lstDeplacements.add(deplacement);
	}

	public Ordonnanceur(Jeu _jeu) {
		jeu = _jeu;
	}

	public void start(long _pause) {
		pause = _pause;
		new Thread(this).start();
	}

	@Override
	public void run() {
		boolean update = false;
		cpt = 0;

		while (true) {
			while (!jeu.gameOver()) {
				cpt++;
				if (jeu.niveauTerminee()) {
					jeu.incrementNombreDeNiveauxJoues();
					while (jeu.getCompteARebourd() > 0) {
						jeu.decrementeCompteARebourd();
						jeu.incrementeScore();

						if (update) {
							setChanged();
							notifyObservers();
						}

						try {
							sleep(1);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					lstDeplacements = new ArrayList<RealisateurDeDeplacement>();
					jeu.nextLevel();
				}

				jeu.resetCmptDepl();
				jeu.decrementeCompteARebourd();
				for (RealisateurDeDeplacement d : lstDeplacements) {

					if (d.realiserDeplacement())
						update = true;

				}

				Controle4Directions.getInstance().resetDirection();

				if (update) {
					setChanged();
					notifyObservers();
				}

				try {
					sleep(pause);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Game Over!");
			resetLstDeplacements();
			jeu.resetGame();
		}
	}

	public int getCpt() {
		return cpt;
	}
}
