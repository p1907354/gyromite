package modele.deplacements;

import modele.plateau.Bot;
import modele.plateau.Corde;
import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

public class IA extends RealisateurDeDeplacement {

    @Override
    protected boolean realiserDeplacement() {
        boolean ret = false;

        for (EntiteDynamique e : lstEntitesDynamiques) {
            if (e.getIdentifiant() == Bot.ID) {
                Bot b = (Bot) e;
                // Entite eCible = b.regarderDansLaDirection(b.getDirection());
                Entite eCachee = b.getEntiteCachee();

                if (b.avancerDirectionChoisie(b.getDirection())) {
                    ret = true;
                } else {
                    b.switchDirection();
                    b.avancerDirectionChoisie(b.getDirection());
                    ret = true;
                }

                if (eCachee != null && eCachee.getIdentifiant() == Corde.ID) {
                    b.setDirection(b.getRandomDirection());
                    while (!(b.avancerDirectionChoisie(b.getDirection()))) {
                        b.setDirection(b.getRandomDirection());
                    }
                    b.avancerDirectionChoisie(b.getDirection());
                }
            }
        }
        return ret;
    }
}
