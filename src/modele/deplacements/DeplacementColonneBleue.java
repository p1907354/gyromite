package modele.deplacements;

public class DeplacementColonneBleue extends DeplacementColonne {
	private static DeplacementColonneBleue dcb;

	public DeplacementColonneBleue(String couleurStr) {
		this.couleurStr = couleurStr;
	}

	public static DeplacementColonneBleue getInstance() {
		if (dcb == null) {
			dcb = new DeplacementColonneBleue("Bleu");
		}

		return dcb;
	}
}
