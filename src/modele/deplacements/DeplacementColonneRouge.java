package modele.deplacements;

public class DeplacementColonneRouge extends DeplacementColonne {
	private static DeplacementColonneRouge dcb;

	public DeplacementColonneRouge(String couleurStr) {
		this.couleurStr = couleurStr;
	}

	public static DeplacementColonneRouge getInstance() {
		if (dcb == null) {
			dcb = new DeplacementColonneRouge("Rouge");
		}

		return dcb;
	}
}