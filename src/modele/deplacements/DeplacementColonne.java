package modele.deplacements;

import modele.plateau.Colonne;
import modele.plateau.ComposantColonne;
import modele.plateau.Connecteur;
import modele.plateau.Entite;

/**
 * A la reception d'une commande, toutes les cases (EntitesDynamiques) des
 * colonnes se déplacent dans la direction définie (vérifier "collisions" avec
 * le héros)
 */
public class DeplacementColonne extends RealisateurDeDeplacement {
	private Direction directionCourante;
	// Design pattern singleton
	private static DeplacementColonne dc;
	protected String couleurStr;
	
	public static DeplacementColonne getInstance() {
		if (dc == null) {
			dc = new DeplacementColonne();
		}
		return dc;
	}

	public void setDirectionCourante() {
		for (Colonne c : mapLstEntitesColonnes.get(couleurStr)) {
			if (c.getDirection() == Direction.haut)
				c.setDirection(Direction.bas);
			else if (c.getDirection() == Direction.bas)
				c.setDirection(Direction.haut);
		}

		directionCourante = mapLstEntitesColonnes.get(couleurStr).get(0).getDirection();
	}

	@Override
	public boolean realiserDeplacement() {
		boolean ret = false;
		for (Colonne c : mapLstEntitesColonnes.get(couleurStr)) {
			if (directionCourante != null) {
				switch (directionCourante) {
				case gauche:
				case droite:
				case haut:
					Entite eGauche = c.getComposants().get(c.getNbComposants() - 1)
							.regarderDansLaDirection(Direction.gauche);
					Entite eDroite = c.getComposants().get(c.getNbComposants() - 1)
							.regarderDansLaDirection(Direction.droite);
					if ((eGauche == null || eGauche != null && eGauche.getIdentifiant() != Connecteur.ID) && 
							(eDroite == null || eDroite != null && eDroite.getIdentifiant() != Connecteur.ID)) {
						for (ComposantColonne cc : c.getComposants()) {
							if (cc.avancerDirectionChoisie(directionCourante)) {
								ret = true;
							}
						}
					}
					break;
				case bas:
					eGauche = c.getComposants().get(0).regarderDansLaDirection(Direction.gauche);
					eDroite = c.getComposants().get(0).regarderDansLaDirection(Direction.droite);
					if ((eGauche == null || eGauche != null && eGauche.getIdentifiant() != Connecteur.ID) && 
							(eDroite == null || eDroite != null && eDroite.getIdentifiant() != Connecteur.ID)) {
						for (int i = c.getComposants().size() - 1; i >= 0; i--) {
							ComposantColonne cc = c.getComposants().get(i);
							if (cc.avancerDirectionChoisie(directionCourante))
								ret = true;
						}
					}
					break;
				}
			}
		}

		return ret;

	}
	
	public void resetDirection() {
		directionCourante = null;
	}
	
	public Direction getDirectionCourante() {
		return directionCourante;
	}
}
