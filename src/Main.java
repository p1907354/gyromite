
import javax.swing.UIManager;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import VueControleur.VueControleurGyromite;
import modele.plateau.Jeu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

@SuppressWarnings("deprecation")
public class Main {
    public static void main(String[] args) throws Exception {
    	UIManager.setLookAndFeel(new NimbusLookAndFeel());
    	
        Jeu jeu = new Jeu();
        
        VueControleurGyromite vc = new VueControleurGyromite(jeu);
        jeu.getOrdonnanceur().addObserver(vc);
        
        vc.setVisible(true);
        jeu.start(150);
    }
}
