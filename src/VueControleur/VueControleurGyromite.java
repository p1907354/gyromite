package VueControleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import modele.deplacements.Controle4Directions;
import modele.deplacements.DeplacementColonneBleue;
import modele.deplacements.DeplacementColonneRouge;
import modele.deplacements.Direction;
import modele.plateau.Bombe;
import modele.plateau.Bot;
import modele.plateau.ComposantColonne;
import modele.plateau.Connecteur;
import modele.plateau.Corde;
import modele.plateau.Heros;
import modele.plateau.Jeu;

@SuppressWarnings("deprecation")

/**
 * Cette classe a deux fonctions : (1) Vue : proposer une représentation
 * graphique de l'application (cases graphiques, etc.) (2) Controleur : écouter
 * les évènements clavier et déclencher le traitement adapté sur le modèle
 * (flèches direction Pacman, etc.))
 *
 */
public class VueControleurGyromite extends JFrame implements Observer {
	private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle
						// pour le rafraichissement, permet de communiquer les actions clavier (ou
						// souris)

	private int sizeX; // taille de la grille affichée
	private int sizeY;

	// icones affichées dans la grille
	private ImageIcon iconHeroDirectionDroite;
	private ImageIcon iconHeroDirectionGauche;
	private ImageIcon iconHeroJumpDroite;
	private ImageIcon iconHeroJumpGauche;
	private ImageIcon iconBot;
	private ImageIcon iconVide;
	private ImageIcon iconPoutreVerticale;
	private ImageIcon iconPoutreHorizontale;
	private ImageIcon iconSol;
	private ImageIcon iconColonneBleue;
	private ImageIcon iconBombe;
	private ImageIcon iconCorde;
	private ImageIcon iconRadis;
	private ImageIcon iconColonneBleueConnecteur;
	private ImageIcon iconColonneBleueConnecteurBas;
	private ImageIcon iconColonneBleueConnecteurHaut;
	private ImageIcon iconColonneBleueBas;
	private ImageIcon iconColonneBleueHaut;
	private ImageIcon iconColonneRougeHaut;
	private ImageIcon iconColonneRouge;
	private ImageIcon iconColonneRougeBas;
	private ImageIcon iconColonneRougeConnecteur;
	private ImageIcon iconColonneRougeConnecteurBas;
	private ImageIcon iconColonneRougeConnecteurHaut;
	private ImageIcon iconConnecteurGauche;
	private ImageIcon iconConnecteurDroite;
	private ImageIcon iconHeroSurCorde;

	private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associée
									// à une icône, suivant ce qui est présent dans le modèle)

	private JPanel grilleJLabels;
	private JPanel controleJPanel = new JPanel(new BorderLayout());

	private JLabel scoreJLabel = new JLabel("SCORE    -    0");
	private JLabel bestScoreJLabel = new JLabel("BEST SCORE    -    0");
	private JLabel timeJLabel = new JLabel("TIME    1000");

	private Dimension dimensionEcran;

	private HashMap<Integer, ImageIcon> mapTextures = new HashMap<>();

	public VueControleurGyromite(Jeu _jeu) {
		sizeX = _jeu.getTailleX();
		sizeY = _jeu.getTailleY();
		jeu = _jeu;

		System.out.println(sizeX + " " + sizeY);
		tabJLabel = new JLabel[sizeX][sizeY];
		grilleJLabels = new JPanel(new GridLayout(sizeY, sizeX));

		placerLesComposantsGraphiques();
		chargerLesIcones();
		remplirMapTextures();
		mettreAJourAffichage();
		ajouterEcouteurClavier();
	}

	private void ajouterEcouteurClavier() {
		grilleJLabels.addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme,
														// il s'agit d'un objet qui correspond au controleur dans MVC
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) { // on regarde quelle touche a été pressée
				case KeyEvent.VK_LEFT:
					Controle4Directions.getInstance().setDirectionCourante(Direction.gauche);
					break;
				case KeyEvent.VK_RIGHT:
					Controle4Directions.getInstance().setDirectionCourante(Direction.droite);
					break;
				case KeyEvent.VK_DOWN:
					Controle4Directions.getInstance().setDirectionCourante(Direction.bas);
					break;
				case KeyEvent.VK_UP:
					Controle4Directions.getInstance().setDirectionCourante(Direction.haut);
					break;
				case KeyEvent.VK_R:
					DeplacementColonneRouge.getInstance().setDirectionCourante();
					break;
				case KeyEvent.VK_B:
					DeplacementColonneBleue.getInstance().setDirectionCourante();
					break;
				}
			}
		});
	}

	private int pasXIcone(String cheminFichier, int nbEltX) {
		ImageIcon icone = chargerIcone(cheminFichier);
		return icone.getIconWidth() / nbEltX;
	}

	private int pasYIcone(String cheminFichier, int nbEltY) {
		ImageIcon icone = chargerIcone(cheminFichier);
		return icone.getIconHeight() / nbEltY;
	}

	private void chargerLesIcones() {
		int w = pasXIcone("data/Images/tileset.png", 5);
		int h = pasYIcone("data/Images/tileset.png", 12);

		System.out.println(w + " " + h);

		iconPoutreHorizontale = chargerIcone("data/Images/tileset.png", 0, 0, w, h);
		iconPoutreVerticale = chargerIcone("data/Images/tileset.png", 0, h, w, h);
		iconSol = chargerIcone("data/Images/tileset.png", w * 2, 0, w, h);
		iconCorde = chargerIcone("data/Images/tileset.png", w, 0, w, h);
		iconVide = chargerIcone("data/Images/tileset.png", w * 3, h, w, h);

		iconColonneBleueHaut = chargerIcone("data/Images/tileset.png", 0, h * 3, w, h);
		iconColonneBleue = chargerIcone("data/Images/tileset.png", w, h * 3, w, h);
		iconColonneBleueBas = chargerIcone("data/Images/tileset.png", w * 2, h * 3, w, h);
		iconColonneBleueConnecteur = chargerIcone("data/Images/tileset.png", w, h * 2, w, h);
		iconColonneBleueConnecteurBas = chargerIcone("data/Images/tileset.png", w * 2, h * 2, w, h);
		iconColonneBleueConnecteurHaut = chargerIcone("data/Images/tileset.png", 0, h * 2, w, h);

		iconColonneRougeHaut = chargerIcone("data/Images/tileset.png", 0, h * 5, w, h);
		iconColonneRouge = chargerIcone("data/Images/tileset.png", w, h * 5, w, h);
		iconColonneRougeBas = chargerIcone("data/Images/tileset.png", w * 2, h * 5, w, h);
		iconColonneRougeConnecteur = chargerIcone("data/Images/tileset.png", w, h * 4, w, h);
		iconColonneRougeConnecteurBas = chargerIcone("data/Images/tileset.png", w * 2, h * 4, w, h);
		iconColonneRougeConnecteurHaut = chargerIcone("data/Images/tileset.png", 0, h * 4, w, h);

		iconConnecteurGauche = chargerIcone("data/Images/tileset.png", w, h, w, h);
		iconConnecteurDroite = chargerIcone("data/Images/tileset.png", w * 2, h, w, h);

		w = pasXIcone("data/Images/smick_ca.png", 4);
		h = pasYIcone("data/Images/smick_ca.png", 5);

		iconBot = chargerIcone("data/Images/smick_ca.png", 0, 0, w, h);
		iconRadis = chargerIcone("data/Images/smick_ca.png", w, h * 4, w, h);

		w = pasXIcone("data/Images/player_ca.png", 6);
		h = pasYIcone("data/Images/player_ca.png", 5);

		iconHeroDirectionGauche = chargerIcone("data/Images/player_ca.png", 0, 0, w, h);
		iconHeroSurCorde = chargerIcone("data/Images/player_ca.png", 0, h * 2, w, h);
		iconHeroJumpGauche = chargerIcone("data/Images/player_ca.png", w * 2, h * 2, w, h);

		w = pasXIcone("data/Images/player_ca_revert.png", 6);
		h = pasYIcone("data/Images/player_ca_revert.png", 5);

		iconHeroDirectionDroite = chargerIcone("data/Images/player_ca_revert.png", w * 5, 0, w, h);
		iconHeroJumpDroite = chargerIcone("data/Images/player_ca_revert.png", w * 3, h * 2, w, h);

		w = pasXIcone("data/Images/bomb_ca.png", 4);
		h = pasYIcone("data/Images/bomb_ca.png", 2);

		iconBombe = chargerIcone("data/Images/bomb_ca.png", 10, 10, 40, 40);
	}

	private void remplirMapTextures() {
		mapTextures.put(3, iconPoutreHorizontale);
		mapTextures.put(4, iconPoutreVerticale);
		mapTextures.put(5, iconSol);
		mapTextures.put(6, iconCorde);
		mapTextures.put(7, iconRadis);
	}

	private void placerLesComposantsGraphiques() {

		dimensionEcran = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

		this.setTitle("Gyromite");
		this.setSize((int) dimensionEcran.getWidth() / 2, (int) dimensionEcran.getHeight() / 2);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container container = this.getContentPane();

		JPanel gamePanel = new JPanel(new BorderLayout());

		gamePanel.add(controleJPanel(), BorderLayout.SOUTH);
		gamePanel.add(grilleJLabels(), BorderLayout.CENTER);

		container.add(gamePanel);

		this.pack();
		this.setLocationRelativeTo(null);
	}

	private JPanel grilleJLabels() {
		grilleJLabels.setPreferredSize(
				new Dimension(((int) dimensionEcran.getWidth() / 2), (int) ((dimensionEcran.getHeight() / 2))));
		grilleJLabels.setMinimumSize(new Dimension(500 * (sizeX / 10), 500 * (sizeY / 10)));
		grilleJLabels.setBorder(new LineBorder(Color.BLACK, 40));

		tabJLabel = new JLabel[sizeX][sizeY];
		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < sizeX; x++) {
				JLabel jlab = new JLabel();
				tabJLabel[x][y] = jlab;
				grilleJLabels.add(jlab);
			}
		}
//      this.add(grilleJLabels);
		grilleJLabels.setFocusable(true);
		grilleJLabels.setRequestFocusEnabled(true);
		return grilleJLabels;
	}

	private JPanel controleJPanel() {
		controleJPanel.setPreferredSize(new Dimension(-1, 100));
		controleJPanel.setMinimumSize(new Dimension(-1, 100));
		controleJPanel.setBackground(Color.BLACK);

		controleJPanel.add(scoreEtTimeJPanel(), BorderLayout.CENTER);
//         controleJPanel.add(boutonParametreJButton(), BorderLayout.EAST);

		controleJPanel.setFocusable(false);
		return controleJPanel;
	}

	private JPanel scoreEtTimeJPanel() {
		JPanel scoreEtTimeJPanel = new JPanel(new GridLayout(1, 2, 100, 100));
		scoreEtTimeJPanel.setBackground(Color.BLACK);

		JLabel titleGameJLabel = new JLabel("GYROMITE");
		titleGameJLabel.setForeground(new Color(128, 255, 0));
		titleGameJLabel.setFont(new Font("", Font.ITALIC, 30));
		titleGameJLabel.setBorder(new LineBorder(Color.BLUE, 5));
		titleGameJLabel.setHorizontalAlignment(SwingConstants.CENTER);

		scoreEtTimeJPanel.add(scoresJLabel());
		scoreEtTimeJPanel.add(titleGameJLabel);
		scoreEtTimeJPanel.add(timeJLabel());

		return scoreEtTimeJPanel;
	}

	private JPanel scoresJLabel() {
		JPanel panel = new JPanel(new GridLayout(2, 1));
		panel.setBackground(Color.BLACK);
				
		scoreJLabel.setBorder(new LineBorder(Color.BLUE, 5));
		scoreJLabel.setForeground(new Color(128, 255, 0));
		scoreJLabel.setFont(new Font("", Font.BOLD, 15));
		scoreJLabel.setHorizontalAlignment(SwingConstants.CENTER);

		bestScoreJLabel.setBorder(new LineBorder(Color.BLUE, 5));
		bestScoreJLabel.setForeground(new Color(128, 255, 0));
		bestScoreJLabel.setFont(new Font("", Font.BOLD, 15));
		bestScoreJLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		panel.add(scoreJLabel);
		panel.add(bestScoreJLabel);
		
		return panel;
	}

	private JLabel timeJLabel() {
		timeJLabel.setBorder(new LineBorder(Color.BLUE, 5));
		timeJLabel.setFont(new Font("", Font.BOLD, 15));
		timeJLabel.setForeground(new Color(128, 255, 0));
		timeJLabel.setHorizontalAlignment(SwingConstants.CENTER);

		return timeJLabel;
	}

	private ImageIcon setIconeColonne(int x, int y, String couleurStr) {
		if ((x - 1 >= 0 && jeu.getGrille()[x - 1][y] != null
				&& jeu.getGrille()[x - 1][y].getIdentifiant() == Connecteur.ID)
				|| (x + 1 < sizeX && jeu.getGrille()[x + 1][y] != null
						&& jeu.getGrille()[x + 1][y].getIdentifiant() == Connecteur.ID)) {
			if (y - 1 < 0 || (jeu.getGrille()[x][y - 1] == null)
					|| !(jeu.getGrille()[x][y - 1].getIdentifiant() == ComposantColonne.ID))
				if (((ComposantColonne)jeu.getGrille()[x][y]).getCouleurStr() == "Bleu")
					return iconColonneBleueConnecteurHaut;
				else
					return iconColonneRougeConnecteurHaut;
			else if ((y + 1 >= sizeY) || (jeu.getGrille()[x][y + 1] == null)
					|| !(jeu.getGrille()[x][y + 1].getIdentifiant() == ComposantColonne.ID))
				if (((ComposantColonne)jeu.getGrille()[x][y]).getCouleurStr() == "Bleu")
					return iconColonneBleueConnecteurBas;
				else
					return iconColonneRougeConnecteurBas;
			else if (((ComposantColonne)jeu.getGrille()[x][y]).getCouleurStr() == "Bleu")
				return iconColonneBleueConnecteur;
			else
				return iconColonneRougeConnecteur;
		} else {
			if (y - 1 < 0 || (jeu.getGrille()[x][y - 1] == null)
					|| !(jeu.getGrille()[x][y - 1].getIdentifiant() == ComposantColonne.ID))
				if (((ComposantColonne)jeu.getGrille()[x][y]).getCouleurStr() == "Bleu")
					return iconColonneBleueHaut;
				else
					return iconColonneRougeHaut;
			else if (y + 1 >= sizeY || (jeu.getGrille()[x][y + 1] == null)
					|| !(jeu.getGrille()[x][y + 1].getIdentifiant() == ComposantColonne.ID))
				if (((ComposantColonne)jeu.getGrille()[x][y]).getCouleurStr() == "Bleu")
					return iconColonneBleueBas;
				else
					return iconColonneRougeBas;
			else if (((ComposantColonne)jeu.getGrille()[x][y]).getCouleurStr() == "Bleu")
				return iconColonneBleue;
			else
				return iconColonneRouge;
		}
	}

	/**
	 * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté
	 * de la vue (tabJLabel)
	 */
	private void mettreAJourAffichage() {

		for (int x = 0; x < sizeX; x++) {
			for (int y = 0; y < sizeY; y++) {
				if (jeu.getGrille()[x][y] == null) {
					tabJLabel[x][y].setIcon(iconVide);
				} else if (jeu.getGrille()[x][y].getIdentifiant() == Heros.ID) {
					Heros h = (Heros) jeu.getGrille()[x][y];
					if (h.getEntiteCachee() != null && h.getEntiteCachee().getIdentifiant() == Corde.ID)
						tabJLabel[x][y].setIcon(iconHeroSurCorde);
					else if (!h.getDirection()) {
						if (!h.toucheLeSol())
							tabJLabel[x][y].setIcon(iconHeroJumpGauche);
						else
							tabJLabel[x][y].setIcon(iconHeroDirectionGauche);
					} else if (!h.toucheLeSol())
						tabJLabel[x][y].setIcon(iconHeroJumpDroite);
					else
						tabJLabel[x][y].setIcon(iconHeroDirectionDroite);
				} else if (jeu.getGrille()[x][y].getIdentifiant() == Bot.ID) {
					tabJLabel[x][y].setIcon(iconBot);
				} else if (jeu.getGrille()[x][y].getIdentifiant() == ComposantColonne.ID && 
						((ComposantColonne) jeu.getGrille()[x][y]).getCouleurStr() == "Rouge") {
					tabJLabel[x][y].setIcon(setIconeColonne(x, y, "Rouge"));
				} else if (jeu.getGrille()[x][y].getIdentifiant() == ComposantColonne.ID && 
						((ComposantColonne) jeu.getGrille()[x][y]).getCouleurStr() == "Bleu") {
					tabJLabel[x][y].setIcon(setIconeColonne(x, y, "Bleu"));
				} else if (jeu.getGrille()[x][y].getIdentifiant() == Bombe.ID) {
					int i = jeu.getOrdonnanceur().getCpt()%4;
					if (i == 0)
						tabJLabel[x][y].setIcon(iconBombe);
				} else if (jeu.getGrille()[x][y].getIdentifiant() == Connecteur.ID) {
					if ((x + 1) < sizeX && jeu.getGrille()[x + 1][y] != null) {
						if ((jeu.getGrille()[x + 1][y].getIdentifiant() == ComposantColonne.ID) && 
							(((ComposantColonne) jeu.getGrille()[x + 1][y]).getCouleurStr() == "Bleu" || 
							((ComposantColonne) jeu.getGrille()[x + 1][y]).getCouleurStr() == "Rouge"))
							tabJLabel[x][y].setIcon(iconConnecteurGauche);
					}
					if (x - 1 >= 0 && jeu.getGrille()[x - 1][y] != null) {
						if ((jeu.getGrille()[x - 1][y].getIdentifiant() == ComposantColonne.ID) && 
							(((ComposantColonne) jeu.getGrille()[x - 1][y]).getCouleurStr() == "Bleu" || 
							((ComposantColonne) jeu.getGrille()[x - 1][y]).getCouleurStr() == "Rouge"))
							tabJLabel[x][y].setIcon(iconConnecteurDroite);
					}
				} else {
					tabJLabel[x][y].setIcon(mapTextures.get(jeu.getGrille()[x][y].getIdentifiant()));
				}
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				timeJLabel.setText("TIME    " + jeu.getCompteARebourd());
				scoreJLabel.setText("SCORE    -    " + jeu.getScore());
				bestScoreJLabel.setText("BEST SCORE    -    " + jeu.getMeilleurScore());
				mettreAJourAffichage();
			}
		});
	}

	// chargement de l'image entière comme icone
	private ImageIcon chargerIcone(String urlIcone) {
		BufferedImage image = null;

		try {
			image = ImageIO.read(new File(urlIcone));
		} catch (IOException ex) {
			Logger.getLogger(VueControleurGyromite.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}

		return new ImageIcon(image);
	}

	// chargement d'une sous partie de l'image
	private ImageIcon chargerIcone(String urlIcone, int x, int y, int w, int h) {
		// charger une sous partie de l'image à partir de ses coordonnées dans urlIcone
		BufferedImage bi = getSubImage(urlIcone, x, y, w, h);
		// adapter la taille de l'image a la taille du composant
		return new ImageIcon(bi.getScaledInstance(grilleJLabels.getWidth() / sizeX, grilleJLabels.getHeight() / sizeY,
				java.awt.Image.SCALE_SMOOTH));
	}

	private BufferedImage getSubImage(String urlIcone, int x, int y, int w, int h) {
		BufferedImage image = null;

		try {
			image = ImageIO.read(new File(urlIcone));
		} catch (IOException ex) {
			Logger.getLogger(VueControleurGyromite.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
		BufferedImage bi = image.getSubimage(x, y, w, h);
		return bi;
	}

}
